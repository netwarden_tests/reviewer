from loguru import logger

from gitlabconnector import GitLabConnector


class GroupManager:
    """_summary_
    """
    def __init__(self, connector: GitLabConnector):
        self.connector = connector

    def create_group(self, name, description=None):
        """_summary_

        Args:
            name (_type_): _description_
            description (_type_, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        try:
            group = self.connector.gl.groups.create({
                'name': name,
                'path': name.lower().replace(' ', '_'),
                'description': description or ''
                })

            return group

        except Exception as e:
            logger.error(f"Ошибка при создании группы: {e}")
            return None

    def create_project_in_group(self, project_name, group_id):
        """_summary_

        Args:
            project_name (_type_): _description_
            group_id (_type_): _description_

        Returns:
            _type_: _description_
        """
        try:
            project = self.connector.gl.projects.create({
                'name': project_name,'namespace_id': group_id
                })

            logger.info(f"Создан проект: {project_name}")
            return project

        except Exception as e:
            logger.error(f"Ошибка при создании проекта: {e}")
            return None

    def delete_project(self, project_id):
        """_summary_

        Args:
            project_id (_type_): _description_
        """
        try:
            project = self.connector.gl.projects.get(project_id)
            project.delete()
            logger.info(f"Проект {project_id} успешно удален")

        except Exception as e:
            logger.error(f"Ошибка при удалении проекта: {e}")

    def get_projects_in_group(self, group_id):
        """_summary_

        Args:
            group_id (_type_): _description_

        Returns:
            _type_: _description_
        """
        try:
            group = self.connector.gl.groups.get(group_id)
            projects = group.projects.list(all=True)

            return projects

        except Exception as e:

            logger.error(f"Ошибка при получении списка \
                         проектов в группе {group_id}: {e}")
            return None

    def delete_all_projects_in_group(self, group_id):
        """_summary_

        Args:
            group_id (_type_): _description_

        Returns:
            _type_: _description_
        """
        try:
            group = self.connector.gl.groups.get(group_id)
            projects = group.projects.list(all=True)
            for project in projects:
                pr = self.connector.gl.projects.get(project.id)
                pr.delete()
                logger.info(f"Проект {project.id} успешно удален")

        except Exception as e:
            logger.error(f"Ошибка удалении проектов в группе {group_id}: {e}")
            return None

def create_test_projects(group_id: int):
    """_summary_

    Args:
        group_id (int): _description_
    """
    connector = GitLabConnector.from_config(config_path = "./config.ini")
    manager = GroupManager(connector)
    proj_names = []
    with open("test/slush") as f:
        for line in f:
            proj_names.append(line.strip())
    for name in proj_names:
        manager.create_project_in_group(name,group_id)

def delete_all_test_projects(group_id: int):
    """_summary_

    Args:
        group_id (int): _description_
    """
    connector = GitLabConnector.from_config(config_path = "./config.ini")
    manager = GroupManager(connector)
    manager.delete_all_projects_in_group(group_id)

def check_che_vnutri(group_id: int):
    connector = GitLabConnector.from_config(config_path = "./config.ini")
    manager = GroupManager(connector)
    projects = manager.get_projects_in_group(group_id)
    print(type(projects[0]))
    print(projects[0])

if __name__ == "__main__":
    group_id = 83630696
    #delete_all_test_projects(group_id)
    #create_test_projects(group_id)
    check_che_vnutri(group_id)