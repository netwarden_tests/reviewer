#!/bin/bash

for i in $(seq 1 50); do
    year=2019
    num=$((3 + RANDOM % 2))
    seq=$i
    letters=$(env LC_CTYPE=C tr -dc 'a-z' < /dev/urandom | head -c 3)
    echo "$year-$num-$seq-$letters"
done
