import configparser
import sys

import gitlab
from loguru import logger


class GitLabConnector:
    def __init__(self, url_git: str, private_token: str, ssl: bool = False) -> None:
        try:
            self.gl = gitlab.Gitlab(url=url_git, private_token= private_token,
                                    ssl_verify= ssl)

        except gitlab.GitlabAuthenticationError:
            logger.error("Ошибка аутентификации. Проверьте права токенра доступа.")
            sys.exit(1)

        except gitlab.GitlabConnectionError:
            logger.error(f"Ошибка соединения. Не удаётся присоединится к {url_git}.")

        except Exception as e:
            logger.error(f"???:{e}")
            sys.exit(1)

    @classmethod
    def from_config(cls, config_path: str) -> None:
        config = configparser.ConfigParser()
        config.read(config_path)

        url_git = config.get('gitlab', 'url')
        private_token = config.get('gitlab','private_token')
        ssl_verify = config.getboolean('gitlab', 'ssl', fallback=False)

        return cls(url_git, private_token, ssl_verify)
