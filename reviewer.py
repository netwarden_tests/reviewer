import re
import pickle
import random
import json
import argparse
import configparser

from gitlabconnector import GitLabConnector
from datetime import datetime, timedelta
from loguru import logger
import gitlab

# Функция для проверки корректности входных файлов (Слушаки only?)
# неактуально
def check_file_format(file_path):
    pattern = re.compile(r'^\d{4}-\d-\d{1,2}-[a-z]{3}$')
    with open(file_path, 'r') as file:
        for line in file:
            line = line.strip()
            if not pattern.match(line):
                return False
    return True

def save_projects_to_file(projects, filename="projects.json"):
    with open(filename, "w") as file:
        json.dump(projects, file, ensure_ascii=False, indent=4)
    logger.info(f"Проекты сохранены в файл {filename}")

def load_projects_from_file(filename="projects.json"):
    try:
        with open(filename, "r") as file:
            projects = json.load(file)
        logger.info(f"Проекты загружены из файла {filename}")
        return projects
    except FileNotFoundError:
        logger.error(f"Файл {filename} не найден")
        return None


def assign_projects_to_reviewers(connector, group_id: int, reviewers_dict: dict = None):

    group = connector.gl.groups.get(group_id)
    projects = group.projects.list(all=True)

    # Да простит меня бог. Всё что написано дальше - кошмар
    filtered_projects = list()
    reviewers_projects = list()
    
    # Разделяем мясо от костей (Проекты проверяющих выкидываем из пула)
    for project in projects:
        if project.name in reviewers_dict:
            reviewers_projects.append([project.name,project.id,project.web_url])
        else:
            filtered_projects.append([project.name,project.id,project.web_url])

    # Считаем сколько добавить проектов в пул для нормальной делюги
    leveler = len(reviewers_dict) - len(filtered_projects) % len(reviewers_dict)
    random.shuffle(filtered_projects)
    # Добиваем кому-то случайный проект на ревью, всем одинаково - всё честно
    filtered_projects.extend(filtered_projects[:leveler])
    # САМАЯ ЖЕСТЬ - ладно, тут пока на потом. Надо переделать сто процентов
    if len(reviewers_projects) > -0:
        reviewers_projects_shift = [reviewers_projects[-1]] + reviewers_projects[:-1]

    parts_list = []
    keys = list(reviewers_dict.values())
    for i in range(len(reviewers_dict)):
        if len(reviewers_projects) != 0:
            key = reviewers_dict[reviewers_projects[i][0]]
            
            start = i * len(filtered_projects) // len(reviewers_projects)
            end = (i + 1) * len(filtered_projects) // len(reviewers_projects)
            #Закидываем проект предыдущего ревьюера в текущего
            
            upgrage_filter = filtered_projects[start:end]
            upgrage_filter.append(reviewers_projects_shift[i])
            parts_list.append([key,upgrage_filter])

        else:
            key = keys[i]
            start = i * len(filtered_projects) // len(reviewers_dict)
            end = (i + 1) * len(filtered_projects) // len(reviewers_dict)
            upgrage_filter = filtered_projects[start:end]
            parts_list.append([key,upgrage_filter])

    print(parts_list)
    return parts_list

def add_user_to_projects(connector, user_projects_data, number_projects=0):
    for user_data in user_projects_data:
        username, projects = user_data[0], user_data[1]
        # Поиск пользователя по username
        try:
            user = connector.gl.users.list(username=username)[0]
        except IndexError:
            logger.error(f"Пользователь {username} не найден.")
            continue
        except Exception as e:
            logger.error(f"Ошибка при поиске пользователя {username}: {e}")
            continue

        if number_projects > 0:
            projects = projects[:number_projects]

        # Добавление пользователя в каждый проект
        for project_data in projects:
            project_name, project_id, project_url = project_data
            try:
                project = connector.gl.projects.get(project_id)
                # Вычисление даты истечения срока действия
                expires_at = (datetime.now() + timedelta(days=1)).date().isoformat()
                # Добавление пользователя в проект
                project.members.create({
                    'user_id': user.id,
                    'access_level': gitlab.DEVELOPER_ACCESS,
                    'expires_at': expires_at
                })
                logger.info(f"Пользователь {username} добавлен в проект {project_name} на 1 сутки.")
            except Exception as e:
                logger.error(f"Ошибка при добавлении пользователя {username} в проект {project_name}: {e}")


def remove_user_from_projects(connector, user_projects_data):
    for username, projects in user_projects_data:
        # Поиск пользователя по username
        try:
            user = connector.gl.users.list(username=username)[0]
        except IndexError:
            logger.error(f"Пользователь {username} не найден.")
            continue
        except Exception as e:
            logger.error(f"Ошибка при поиске пользователя {username}: {e}")
            continue
        
        # Удаление пользователя из каждого проекта
        for project_name, project_id, _ in projects:
            try:
                project = connector.gl.projects.get(project_id)
                member = project.members.get(user.id)
                member.delete()
                logger.info(f"Пользователь {username} удален из проекта {project_name}.")
            except gitlab.exceptions.GitlabGetError:
                logger.warning(f"Пользователь {username} не найден в проекте {project_name}.")
                continue  
            except Exception as e:
                logger.error(f"Ошибка при удалении пользователя {username} из проекта {project_name}: {e}")
                


def do_partition(config_path: str, sereal_file: str):
    config = configparser.ConfigParser()
    config.read(config_path)

    group_id = config.get('review','review_group')
    reviewers_path = config.get('review','reviewers')
    connector = GitLabConnector.from_config(config_path)

    reviewers_dict = {}
    with open(reviewers_path) as f:
        for line in f:
            parts = line.strip().split(';')
            if len(parts) == 3:
                key, value = parts[1], parts[2]
                reviewers_dict[key] = value.strip()
            
    parts_list = assign_projects_to_reviewers(connector, group_id, reviewers_dict)
    save_projects_to_file(parts_list, sereal_file)
    #add_user_to_projects(connector, parts_list)

def add_reviewers_to_projects(config_path: str, sereal_file: str):
    config = configparser.ConfigParser()
    config.read(config_path)

    connector = GitLabConnector.from_config(config_path)
    
    parts_list = load_projects_from_file(sereal_file)
    print(parts_list)
    #add_user_to_projects(connector, parts_list)

def remove_reviewers_from_projects(config_path: str, sereal_file: str):
    config = configparser.ConfigParser()
    config.read(config_path)

    connector = GitLabConnector.from_config(config_path)
    
    parts_list = load_projects_from_file(sereal_file)
    print(parts_list)
    remove_user_from_projects(connector, parts_list)
    
def main():
    parser = argparse.ArgumentParser(description="CLI для управления dipl_review")
    parser.add_argument("-m","--mode", choices=["create","add","remove"], required= True, help = "Режимы работы программы")
    parser.add_argument("-s","--serialized", required= True, help = "Файл для хранения данных о разбинии проектов по рецензорам.")
    parser.add_argument("-c","--config", required= True, help = "Конфигурационный файл.")
    parser.add_argument("-n","--number", required= False, default= 0, help = "Колличество проектов выделяемых ревьюеру на ревью. Используется только в add mode.")

    args = parser.parse_args()
    
    if args.mode == "create":
        do_partition(args.config, args.serialized)
    elif args.mode == "add":
        add_reviewers_to_projects(args.config, args.serialized, args.number)
    elif args.mode == "remove":
        remove_reviewers_from_projects(args.config, args.serialized)
        
if __name__ == "__main__":
    #sereal_file = "parts_list.dat"
    #do_partition("./config.ini", sereal_file)
    #add_reviewers_to_projects("./config.ini", sereal_file)
    #remove_reviewers_from_projects("./config.ini", sereal_file)
    main()
    
